(define-module (heat-pde)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake)
  #:use-module (melissa-api)
  #:use-module (gnu packages python)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages commencement)
  #:use-module (guix licenses))

(define-public heat-pde
  (package
   (inherit melissa-api)
   (name "heat-pde")
   (build-system cmake-build-system)
    (inputs (list melissa-api openmpi gcc-toolchain gfortran-toolchain python pkg-config))
    (arguments
     '(
       #:tests? #f
		#:phases
		(modify-phases %standard-phases
			       (add-after 'unpack 'change-dir
					  (lambda _
					    (chdir "./examples/heat-pde/executables"))))
		))
    (synopsis "Instrumented heat-pde use case for Melissa")
))
;; Return the package object define above at the end of the module.
heat-pde
