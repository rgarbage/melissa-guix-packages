(define-module (python-iterative-statistics)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system python)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (guix licenses))

(define-public python-iterative-statistics
  (package
   (name "python-iterative-statistics")
   (version "0.1.0")
   (source (origin
            (method git-fetch)
            (uri (git-reference 
                  (url "https://github.com/IterativeStatistics/BasicIterativeStatistics")
                  (commit "66cacec5756c855f8efcf260a6fb5d7217b9d434")))
            (sha256
		(base32 "02abpqgyhj9wmf7ycafmpax7yx6rckildswxfa4hr6b95jw91352"))))
   (build-system python-build-system)
   (inputs (list python python-numpy))
   (arguments
    '(
      #:tests? #f
	       #:phases (modify-phases %standard-phases (delete 'sanity-check))))
   (home-page "https://github.com/IterativeStatistics/BasicIterativeStatistics")
   (synopsis "Iterative Statistics Pyhton Library")
   (description
    "Implements iterative statistics operators for mean, variance, high-order moments, extrema, covariance, threshold, quantile (experimental) and Sobol' indices"
    )
   (license bsd-3)))

;; Return the package object define above at the end of the module.
python-iterative-statistics
