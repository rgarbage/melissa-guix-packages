(define-module (py-melissa-core)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system python)
  #:use-module (gnu packages python)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-science)
  #:use-module (melissa-api)
  #:use-module (python-iterative-statistics)
  #:use-module (guix licenses))

(define-public py-melissa-core
  (package
   (inherit melissa-api)
   (name "py-melissa-core")
   (build-system python-build-system)
   (inputs (list python python-mpi4py python-pyzmq python-numpy python-jsonschema python-rapidjson python-scipy python-cloudpickle python-iterative-statistics))
   (arguments
    '(
      #:tests? #f
	       #:phases (modify-phases %standard-phases (delete 'sanity-check))))
   (synopsis "Melissa Python server and launcher")
   ))
;; Return the package object define above at the end of the module.
py-melissa-core
