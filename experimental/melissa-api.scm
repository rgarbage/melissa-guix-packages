;; build from  the channel melissa-api.scm file
;; but changed to work in local git repo for development puropse
;; After  cloning a melissa repo, just do within the repo root dir:
;; guix package -f melissa-api.scm
;; This should compile and install  a "melissa-api dev" module
;; Still not 100% sure this is the right way to go for dev
;; See also  https://guix.gnu.org/blog/2023/from-development-environments-to-continuous-integrationthe-ultimate-guide-to-software-development-with-guix/

(define-module (melissa-api)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages networking)
  #:use-module (guix licenses))


(define vcs-file?
  ;; Return true if the given file is under version control.
  (or (git-predicate (current-source-directory))
      (const #t)))

(define-public melissa-api
  (package
   (name "melissa-api")
   (version "dev")
;;   (source (origin
;;            (method git-fetch)
;;            (uri (git-reference
;;                  (url "https://gitlab.inria.fr/melissa/melissa.git")
;;                  (commit "090b0b61df0ca2f0ae0453bea75749cd3d5f9198")))
;;            (sha256
;;             (base32
   ;;              "1zv36r8gbb92v9s6vvzgdvv8ryli6a51b0i3g61k1rr6l3bvh6g3"))))
   (source (local-file "." "melissa-checkout"
		#:recursive? #t
                #:select? vcs-file?))
   (build-system cmake-build-system)
   (inputs (list openmpi zeromq gcc-toolchain gfortran-toolchain pkg-config))
   (arguments
    '(
      #:tests? #f))
   (home-page "https://gitlab.inria.fr/melissa/melissa")
   (synopsis "Melissa API for client instrumentation")
   (description
    "Melissa is a file-avoiding, adaptive, fault-tolerant and elastic
      framework, to run large-scale sensitivity analysis or deep-surrogate
      training on supercomputers.
      This package builds the API used when instrumenting the clients." )
   (license bsd-3)
   )
  )

;; Return the package object define above at the end of the module.
melissa-api
