;; custom channel for melissa
(cons  (channel
        (name 'melissa)
        (url "https://gitlab.inria.fr/melissa/guix-packages.git")
	(branch "master"))
       %default-channels)
